This project actually involves a main page which links to 4 other pages i.e Registration for student page, Registration for Admin page, Login for admin and Student respectively.

So there are 4 folders in my page.
1.html
2.css
3.javaScript
4.img

html: This folder contains the main page in html format and other pages in php format, The PHP files for database connection are also located in the same folder.
      Description of files in the html folder:
        ->main.html: Just a simple webpage which links to other pages (Better to open this file in a localserver).
        ->registration_admin.php: Registration page for Admin (Should be opened in a localserver to see its operation).
        ->registration_student.php: Registration page for Student (Should be opened in a localserver to see its operation).
        ->login_student.php: Login page for student.
        ->login_admin.php: Login page for admin.
        ->form_student_proc.php: Php file for database connection to mySQL in case of registration form for student.
        ->